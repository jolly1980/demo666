package com.example.test666;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test666Application {

	public static void main(String[] args) {
		SpringApplication.run(Test666Application.class, args);
	}

}

